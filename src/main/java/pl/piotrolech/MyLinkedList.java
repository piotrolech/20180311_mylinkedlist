package pl.piotrolech;

public class MyLinkedList implements NodeList {

    private ListItem root = null;

    public MyLinkedList(ListItem root) {
        this.root = root;
    }

    public ListItem getRoot() {
        return this.root;
    }

    public boolean addItem(ListItem newItem) {
        if (this.root == null) {
            this.root = newItem;
            return true;
        }

        ListItem currentItem = this.root;
        while (currentItem != null) {
            int comparator = currentItem.compareTo(newItem);
            if (comparator < 0) { //newItem is greater than currentItem
                if (currentItem.next() != null) {
                    currentItem = currentItem.next();
                } else {
                    currentItem.setNext(newItem).setPrevious(currentItem);
                    return true;
                }
            } else if (comparator > 0) { //newItem is less than currentItem
                if (currentItem.previous() != null) {
                    currentItem.previous().setNext(newItem).setPrevious(currentItem.previous());
                    newItem.setNext(currentItem).setPrevious(newItem);
                } else {
                    newItem.setNext(this.root);
                    this.root.setPrevious(newItem);
                    this.root = newItem;
                }
                return true;
            } else {
                System.out.println(newItem.getValue() + " is already on the list");
                return false;
            }
        }
        return false;
    }

    public boolean removeItem(ListItem itemToDelete) {
        if (itemToDelete != null) {
            System.out.println("Deleting item " + itemToDelete.getValue());
        }

        ListItem currentItem = this.root;
        while (currentItem != null) {
            int comparator = currentItem.compareTo(itemToDelete);
            if (comparator == 0) { // we've found the item to delete
                if (currentItem == this.root) {
                    this.root = currentItem.next();
                } else {
                    currentItem.previous().setNext(currentItem.next());
                    if (currentItem.next() != null) {
                        currentItem.next().setPrevious(currentItem.previous());
                    }
                }
                return true;
            } else if (comparator < 0) {
                currentItem = currentItem.next();
            } else {
                System.out.println("The item " + itemToDelete.getValue() + " is not on the list");
                return false;
            }
        }
        return false;
    }

    public void traverse(ListItem root) {
        if (root == null) {
            System.out.println("The list is empty");
        } else {
            while (root != null) {
                System.out.println(root.getValue().toString());
                root = root.next();
            }
        }
    }
}
