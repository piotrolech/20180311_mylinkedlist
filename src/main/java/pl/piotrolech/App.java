package pl.piotrolech;

public class App {
    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList(null);
        BinarySearch binarySearchList = new BinarySearch(null);
        list.traverse(list.getRoot());

        String stringData = "5 7 3 9 8 2 1 0 4 6 0";
        String[] data = stringData.split(" ");
        for (String s : data) {
            list.addItem(new Node(s));
            binarySearchList.addItem(new Node(s));
        }
        list.traverse(list.getRoot());
        System.out.println("**************");
        list.removeItem(new Node("0"));
        list.traverse(list.getRoot());
        list.removeItem(new Node("0"));
        list.traverse(list.getRoot());
        list.removeItem(new Node("4"));
        list.traverse(list.getRoot());
        System.out.println("_______________________________");
        binarySearchList.removeItem(new Node("9"));
        binarySearchList.traverse(binarySearchList.getRoot());

    }
}
